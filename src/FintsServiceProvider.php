<?php

namespace Welanco\Fints;

use Illuminate\Routing\Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;
use Spatie\Permission\Contracts\Role as RoleContract;
use Spatie\Permission\Contracts\Permission as PermissionContract;

class FintsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        
        $this->publishes([
            __DIR__.'/../config/laravel-fints.php' => config_path('laravel-fints.php'),
        ], 'config');

        $this->loadMigrationsFrom(
            __DIR__.'/../database/migrations'
        );


        //if ($this->app->runningInConsole()) {
        //    $this->commands([
        //        Commands\CreateRole::class,
        //        Commands\CreatePermission::class,
        //    ]);
        //}

        //$this->registerModelBindings();
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/laravel-fints.php',
            'laravel-fints'
        );
    }

    protected function registerModelBindings()
    {
        $config = $this->app->config['permission.models'];

        $this->app->bind(PermissionContract::class, $config['permission']);
        $this->app->bind(RoleContract::class, $config['role']);
    }
}
