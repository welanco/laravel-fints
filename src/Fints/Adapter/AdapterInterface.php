<?php

namespace Welanco\Fints\Adapter;

use Welanco\Fints\Message\AbstractMessage;

/**
 * Interface AdapterInterface
 * @package Welanco\Fints\Adapter
 */
interface AdapterInterface
{
    /**
     * @param AbstractMessage $message
     * @return string
     */
    public function send(AbstractMessage $message);
}
