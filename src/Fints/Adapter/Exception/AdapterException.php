<?php

namespace Welanco\Fints\Adapter\Exception;

/**
 * Class AdapterException
 * @package Welanco\Fints\Adapter\Exception
 */
class AdapterException extends \Exception
{

}
