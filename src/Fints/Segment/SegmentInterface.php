<?php

namespace Welanco\Fints\Segment;

/**
 * Interface SegmentInterface
 * @package Welanco\Fints\Segment
 */
interface SegmentInterface
{
    /**
     * Returns string representation of object.
     *
     * @return string
     */
    public function __toString();

    /**
     * Gets the name of the segment.
     *
     * @return string
     */
    public function getName();
}
