<?php

namespace Welanco\Fints\DataElementGroups;

use Welanco\Fints\Deg;

/**
 * Class SecurityIdentificationDetails
 * @package Welanco\Fints\DataElementGroups
 */
class SecurityIdentificationDetails extends Deg
{
    const PARTY_MS = 1;   // sender
    const CID_NONE = '';

    /**
     * SecurityIdentificationDetails constructor.
     *
     * @param string $cid
     * @param int $systemId
     */
    public function __construct($cid = self::CID_NONE, $systemId = 0)
    {
        $this->addDataElement(static::PARTY_MS);
        $this->addDataElement($cid);
        $this->addDataElement($systemId);
    }
}
