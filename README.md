# Laravel-FinTS (ALPHA) #

A Laravel-Package to integrate onlinebanking-support over FinTS-API in Laravel-Applications.

This Package based on the great ["FinTS HBCI PHP"-Library](https://github.com/mschindler83/fints-hbci-php) from Markus Schindler.

### What is this repository for? ###

* Laravael 5.7 and up (perhaps older Versions, but not tested - please tell me)
* Version 0.1

### Released features ###

* Complete DB with german banks and BIC (Bank Identifyer Code)

### Planned features ###

* REST-API to simple include in Laravel-Projects like shops or other
* Complete database-set to manage banking-accounts for users / consumers

### How do I get it? ###

#### Install via composer: ####

!!! This is my favorite Way !!!

```bash
composer require welanco/laravel-fints
```
#### or Clone via Git ####

The git is hosted on Bitbukkit under [https://bitbucket.org/welanco/laravel-fints](https://bitbucket.org/welanco/laravel-fints).

```bash
cd YourLaravelProjectFolder/vendor
YourLaravelProjectFolder/vendor/ mkdir welanco
YourLaravelProjectFolder/vendor/ cd welanco
YourLaravelProjectFolder/vendor/welanco/ git clone https://bitbucket.org/welanco/laravel-fints.git
YourLaravelProjectFolder/vendor/welanco/ cd ../..
YourLaravelProjectFolder/ composer update
```

### How do I set it up? ###

After the first Step "How do I get it?" You must run the database migrations in Your laravel ProjectFolder.

```bash
cd YourLaravelProjectFolder
YourLaravelProjectFolder/ php artisan migrate
```

### Who do I talk to? ###

Please tell me bugs, enhancements, proposals or other tasks only other the 
[Bitbukkit-Issue-Tracker](https://bitbucket.org/welanco/laravel-fints/issues).

The URL for the Issue-Tracker is:
https://bitbucket.org/welanco/laravel-fints/issues

## On the End ##

This Package is in DEVELOPMENT!!!