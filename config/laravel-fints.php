<?php

return [
    /*
    |--------------------------------------------------------------------------
    | HBCI / FinTS Url
    | can be found here: https://www.hbci-zka.de (use the PIN/TAN URL)
    | Before you can use the URL-Lists, you must Register your Application
    |--------------------------------------------------------------------------
    | Default-URL is the FinTS-URL for KSK-Stendal
    |
    */

    //'fints_server' => env('FINTS_BANK_URL', 'https://banking-st5.s-fints-pt-st.de/fints30'),


    /*
     * By default all permissions will be cached for 24 hours unless a permission or
     * role is updated. Then the cache will be flushed immediately.
     */

    'cache_expiration_time' => 60 * 24,

];
