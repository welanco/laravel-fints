<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFintsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // @TODO - Changing Tables for FinTS
        Schema::create('fints_banks_blzs', function (Blueprint $table) {
            $table->unsignedInteger('blz');
            $table->integer('feature');
            $table->string('name');
            $table->string('postcode');
            $table->string('city');
            $table->string('short_name');
            $table->integer('pan')->nullable();
            $table->string('bic', 11)->nullable();
            $table->string('pm', 2);
            $table->string('record_number', 6);
            $table->string('change_flag', 1);
            $table->boolean('blz_delete_flag');
            $table->unsignedInteger('succession_blz')->nullable();
            $table->timestamps();

            $table->primary('blz', 'fints_banks_blzs_blz_primary');
        });

        Schema::create('fints_banks_urls', function (Blueprint $table) {
            $table->unsignedInteger('blz');
            $table->string('type');
            $table->string('url');
            $table->string('port');
            $table->timestamps();

            $table->foreign('blz')
                ->references('blz')
                ->on('fints_banks_blzs')
                ->onDelete('cascade');

            $table->primary(['blz', 'type'], 'fints_banks_blzs_blz_type_primary');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fints_banks_blzs');
        Schema::drop('fints_banks_urls');
    }
}
